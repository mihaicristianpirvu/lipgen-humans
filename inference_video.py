import cv2
import os
import argparse
import subprocess
import tempfile
import numpy as np
from tqdm import tqdm
from functools import partial
from neural_wrappers.pytorch import device
from media_processing_lib.image import tryReadImage, imgResize, imgResize_batch
from media_processing_lib.video import tryReadVideo
from media_processing_lib.audio import tryReadAudio
from media_processing_lib.audio.melspectrogram import melspectrogram
from vke.face import vidGetFaceBbox, vidGetFaceKeypoints
from vke.utils import vidSmoothExtremes
from readers.utils import getMelChunks
from readers.video_dir_plus_keypoints_reader import coord_to_mask

def getMaskedImage(img):
	h, w = img.shape[0 : 2]
	img[h//2:] = 0
	return img

def datagen(video, mels):
	img_batch, mel_batch, frame_batch, coords_batch, kp_batch = [], [], [], [], []
	img_size = 96
	wav2lip_batch_size = 128

	faceBbox = vidSmoothExtremes(video, vidGetFaceBbox(video, faceLib="face_alignment"))
	faces = [frame[y1:y2, x1:x2] for (frame, (x1, y1, x2, y2)) in zip(video, faceBbox)]
	faces = imgResize_batch(faces, width=img_size, height=img_size, interpolation="bilinear")
	faceKps = vidGetFaceKeypoints(faces, faceLib="face_alignment")
	faceKpsMask = np.expand_dims(coord_to_mask(faceKps, H=img_size, W=img_size), axis=-1)

	def doYield(img_batch, mel_batch, frame_batch, coords_batch, kp_batch):
		img_batch, mel_batch, kp_batch = np.float32(img_batch), np.float32(mel_batch), np.float32(kp_batch)
		img_masked = np.stack([getMaskedImage(img) for img in img_batch.copy()], axis=0)

		img_batch = np.concatenate((img_masked / 255, img_batch / 255), axis=3)
		# img_batch = np.concatenate((img_masked / 255, img_batch / 255, kp_batch), axis=3)
		return img_batch, mel_batch, frame_batch, coords_batch

	for i in tqdm(range(len(mels)), desc="Datagen"):
		frameIx = i % len(video)
		m = mels[i].copy()
		frame_to_save = video[frameIx].copy()
		faceCoords = faceBbox[frameIx].copy()
		face = faces[frameIx]
		kp = faceKpsMask[frameIx]

		img_batch.append(face)
		mel_batch.append(m)
		frame_batch.append(frame_to_save)
		coords_batch.append(faceCoords)
		kp_batch.append(kp)

		if len(img_batch) >= wav2lip_batch_size:
			yield doYield(img_batch, mel_batch, frame_batch, coords_batch, kp_batch)
			img_batch, mel_batch, frame_batch, coords_batch, kp_batch = [], [], [], [], []

	if len(img_batch) > 0:
		yield doYield(img_batch, mel_batch, frame_batch, coords_batch, kp_batch)

def doInferenceVideo(model, video, fps, wav, audiofile, outfile):
	mel_step_size = 16
	batch_size = 128
	fd, tmpOutfile = tempfile.mkstemp(suffix=".mp4")
	mel = melspectrogram(wav)
	mel_chunks = getMelChunks(mel, mel_step_size, fps)

	print("[Wav2Lip] Mel spectrogram: %s. Mel chunks: %s" % (str(mel.shape), str(mel_chunks.shape)))
	full_frames = video[0 : len(mel_chunks)]

	gen = datagen(full_frames, mel_chunks)
	frame_h, frame_w = full_frames[0].shape[:-1]
	out = cv2.VideoWriter(tmpOutfile, cv2.VideoWriter_fourcc(*'mp4v'), fps, (frame_w, frame_h), True)
	Range = tqdm(gen, total=int(np.ceil(float(len(mel_chunks))/batch_size)), desc="Wav2Lip generating")
	for i, (img_batch, mel_batch, frames, coords) in enumerate(Range):
		audio_sequences = np.expand_dims(mel_batch, axis=1)
		face_sequences = img_batch.transpose(0, 3, 1, 2)

		Y = model.npForward(audio_sequences, face_sequences)
		wav2LipPrediction = np.uint8(Y.transpose(0, 2, 3, 1) * 255)

		for w2l, f, c in zip(wav2LipPrediction, frames, coords):
			x1, y1, x2, y2 = c
			w2l = imgResize(w2l, height=y2 - y1, width=x2 - x1, interpolation="bilinear")

			frame = f.copy()
			height = y2 - y1
			hStart = y1 + height // 2
			frame[hStart:y2, x1:x2] = w2l[height//2 :]

			# if i == 3:
			# 	import matplotlib.pyplot as plt
			# 	plt.imshow(f)
			# 	plt.figure()
			# 	plt.imshow(w2l)
			# 	plt.figure()
			# 	plt.imshow(frame)
			# 	plt.show()

			out.write(frame[:, :, ::-1])
	out.release()

	command = "ffmpeg -loglevel panic -y -i %s -i %s -strict -2 -q:v 1 %s" % (audiofile, tmpOutfile, outfile)
	subprocess.call(command, shell=True)
	os.close(fd)
	os.remove(tmpOutfile)
	print("[Wav2Lip] Written result at %s" % outfile)

	result = tryReadVideo(outfile, vidLib="pims")
	return result

def doInferenceImage(model, image, fps, wav, audiofile, outfile):
	# This guarantees that we can do the same process for 1 image or 1 video (broadcast_to just views)
	video = np.broadcast_to(image, (len(wav), *image.shape))
	return doInferenceVideo(model, video, fps, wav, audiofile, outfile)

def getArgs():
	parser = argparse.ArgumentParser(description='Inference code to lip-sync videos in the wild using Wav2Lip models')

	parser.add_argument('checkpoint_path', type=str, 
						help='Name of saved checkpoint to load weights from')
	parser.add_argument('face', type=str, 
						help='Filepath of video/image that contains faces to use')
	parser.add_argument('audio', type=str, 
						help='Filepath of video/audio file to use as raw audio source')
	parser.add_argument('outfile', type=str, help='Video path to save result. See default for an e.g.')
	args = parser.parse_args()
	return args

def inference_video(model, sourcePath, wavPath, resultPath):
	# wav = load_wav(wavPath, 16000)
	wav = tryReadAudio(wavPath, sr=16000)
	if sourcePath.split('.')[-1] in ("jpg", "png", "jpeg"):
		image = tryReadImage(sourcePath, imgLib="opencv")
		fps = 25
		video = doInferenceImage(model, image, fps, wav, wavPath, resultPath)
	elif sourcePath.split(".")[-1] in ("mp4", "mov"):
		video = tryReadVideo(sourcePath, vidLib="pims")
		video = doInferenceVideo(model, video, video.fps, wav, wavPath, resultPath)
	else:
		assert False

	print("Result:", video.shape)

def main():
	from models import Wav2Lip
	args = getArgs()
	wav = tryReadAudio(wavPath, sr=16000)
	model = Wav2Lip()
	model.loadWeights(args.checkpoint_path)
	model.eval()
	inference_video(model, args.face, args.audio, args.outfile)

if __name__ == '__main__':
	main()
