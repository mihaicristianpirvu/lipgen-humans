import numpy as np
import torch as tr
import torch.nn as nn
from neural_wrappers.pytorch import NWModule, device
from neural_wrappers.callbacks import PlotMetrics, SaveModels
from .base_models import SyncNet

def cosine_loss(a, v, y):
	logloss = nn.BCELoss()
	d = nn.functional.cosine_similarity(a, v)
	loss = logloss(d + np.spacing(1), y)
	return loss

def doLoad(model, path):
	checkpoint = tr.load(path, map_location=device)
	s = checkpoint["state_dict"]
	new_s = {}
	for k, v in s.items():
			new_s[k.replace('module.', '')] = v
	model.load_state_dict(new_s)
	print("[BaseModel] Loaded model from %s" % path)

class BaseModel(NWModule):
	def __init__(self, config):
		if config["expertSyncLoss"]:
			expertPath = config["expertPath"]
			del config["expertPath"]
		super().__init__(hyperParameters=config)

		self.audioEncoder = None
		self.faceEncoder = None
		self.faceDecoder = None
		self.baseSyncNet = None

		metrics = {
			"L1 RGB" : lambda y, t, **k : np.abs(y["targetGenerated"] - t["targetFrame"]).mean() * 255,
		}
		callbacks = [SaveModels("best", "L1 RGB")]
		if config["expertSyncLoss"]:
			self.baseSyncNet = SyncNet(dIn=3, T=5)
			doLoad(self.baseSyncNet, expertPath)
			for p in self.baseSyncNet.parameters():
				p.requires_grad = False
			self.baseSyncNet.eval()
			metrics["Expert Sync Loss"] = lambda y, t, **k : y["expertSyncLoss"]
			metrics["SumLosses"] = lambda y, t, **k : \
				(y["expertSyncLoss"] + np.abs(y["targetGenerated"] - t["targetFrame"]).mean() * 255)
			callbacks.append(SaveModels("best", "SumLosses"))
		callbacks.append(PlotMetrics(["Loss", *list(metrics.keys())]))
		self.addMetrics(metrics)
		self.addCallbacks(callbacks)

	def expertSyncLoss(self, mel, g):
		if self.baseSyncNet is None:
			return tr.tensor([0]).to(device)
		self.baseSyncNet.eval()
		# mel :: torch.Size([10, 1, 80, 16])
		# g :: torch.Size([10, 3, T, 96, 96]). However we have [10, 3, 96, 96], so unsqueeze and pad.
		if len(g.shape) == 4:
			g = g.unsqueeze(dim=2)
		if g.shape[2] == 1:
			g = g.repeat(1, 1, self.baseSyncNet.T, 1, 1)
		# Keep only bottom half
		g = g[:, :, :, g.size(3)//2:]
		g = tr.cat([g[:, :, i] for i in range(self.baseSyncNet.T)], dim=1)
		# B, 3 * T, H//2, W
		a, v = self.baseSyncNet(mel, g)
		y = tr.ones(g.size(0)).float().to(device)
		res = cosine_loss(a, v, y)
		return res

	# Regardless of the training algorithm implementation, a LipGen should always generate data from
	#  the inputs source_frame, targetFrame and targetMel. This can/should be overwritten by more specific models.
	def forward(self, audio_sequences, face_sequences):
		audioEmbedding = self.audioEncoder(audio_sequences)
		faceEmbeddingList = self.faceEncoder(face_sequences)
		faceEmbedding = faceEmbeddingList[-1]
		g = self.faceDecoder(faceEmbeddingList, audioEmbedding)
		return g
