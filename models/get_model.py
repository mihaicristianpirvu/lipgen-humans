from typing import Dict

def getModel(config:Dict):
    # From the internets
    if config["modelCode"] == "wav2lip_repo":
        from .wav2lip.repo import Model
    # Wav2Lip reimplementation by me
    elif config["modelCode"] == "wav2lip":
        from .wav2lip.reproduce import Model
    elif config["modelCode"] == "wav2lip_ft":
        from .wav2lip.ft import Model
    # Our algorithms
    elif config["modelCode"] == "lipgen-v1":
        from .lipgen import LipGenV1 as Model
    elif config["modelCode"] == "lipgen-v2":
        from .lipgen import LipGenV2 as Model
    elif config["modelCode"] == "wav2lip_nosigmoid":
        from .wav2lip.nosigmoid import Model
    elif config["modelCode"] == "wav2lip_progressive_sync_v1":
        from .wav2lip.progressive_sync_v1 import Model
    elif config["modelCode"] == "wav2lip_progressive_sync_v2":
        from .wav2lip.progressive_sync_v2 import Model
    # Active experiments
    elif config["modelCode"] == "wav2lip_kp_v1":
        from .wav2lip_kp.v1 import Model
    else:
        assert False

    return Model(config)