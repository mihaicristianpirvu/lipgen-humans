from neural_wrappers.pytorch import NWModule
from overrides import overrides

class LipGAN(NWModule):
	def __init__(self, config):
		super().__init__(hyperParameters=config)

	@overrides
	def setOptimizer(self, optimizer, **kwargs):
		assert not isinstance(optimizer, optim.Optimizer)
		ganOptimizer = GANOptimizer(self, optimizer, **kwargs)
		super().setOptimizer(ganOptimizer, **kwargs)

	@overrides
	def networkAlgorithm(self, trInputs, trLabels, isTraining, isOptimizing):
		assert False, "TODO"
