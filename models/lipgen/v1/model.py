# v1 
import torch as tr
from overrides import overrides
from .audio_encoder import AudioEncoder
from .face_encoder import FaceEncoder
from .face_decoder import FaceDecoder
from ...base_model import BaseModel
from neural_wrappers.pytorch import device

def reconstructionLoss(y, t):
	return (y - t).abs().mean()

class Model(BaseModel):
	def __init__(self, config):
		super().__init__(config)
		self.audioEncoder = AudioEncoder()
		self.faceEncoder = FaceEncoder()
		self.faceDecoder = FaceDecoder()

	@overrides
	def networkAlgorithm(self, trInputs, trLabels, isTraining, isOptimizing):
		B, D, H, W = trInputs["targetFrame"].shape
		mask = tr.ones(H, W).to(device)
		mask[H // 2:] = 0
		mel = trInputs["targetMel"].unsqueeze(dim=1)

		def f(a, t, s):
			# audioSequences :: (B, 1, 80, 16)
			audioSequences = a
			# faceSequences :: (B, 6, 96, 96)
			faceSequences = tr.cat([t * mask, s], dim=1)
			faceEmbeddingList = self.faceEncoder(faceSequences)
			audioEmbedding = self.audioEncoder(audioSequences)
			faceEmbedding = faceEmbeddingList[-1]
			g = self.faceDecoder(faceEmbeddingList, audioEmbedding)
			return g

		g = f(mel, trInputs["targetFrame"], trInputs["sourceFrame"])
		gt = trInputs["targetFrame"]
		gLoss = reconstructionLoss(g, gt)

		for i in range(self.hyperParameters["context"] * 2):
			gCtx = f(trInputs["targetMelContext"][:, i : i + 1], \
				trInputs["targetContext"][:, i], trInputs["sourceContext"][:, i])
			gtCtx = trInputs["targetContext"][:, i]
			gLossCtx = reconstructionLoss(gCtx, gtCtx)
			gLoss += gLossCtx
		gLoss /= (self.hyperParameters["context"] * 2 + 1)

		# gt :: (B, 3, 96, 96)
		gLoss = reconstructionLoss(g, gt)
		self.updateOptimizer(gLoss, isTraining, isOptimizing)

		targetGenerated = g.detach()
		syncLoss = self.expertSyncLoss(mel, targetGenerated)

		trResults = {
			"targetMasked" : trInputs["targetFrame"] * mask,
			"targetGenerated" : targetGenerated,
			"GLoss" : gLoss,
			"expertSyncLoss" : syncLoss
		}

		return trResults, gLoss
