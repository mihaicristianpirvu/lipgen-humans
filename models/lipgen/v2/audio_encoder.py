import torch as tr
import torch.nn as nn
from ...base_models.conv import Conv2d

class AudioEncoder(nn.Module):
	def __init__(self, dIn=1):
		super(AudioEncoder, self).__init__()
		self.dIn = dIn

		self.model = nn.Sequential(
			Conv2d(dIn, 32, kernel_size=3, stride=1, padding=1),
			Conv2d(32, 32, kernel_size=3, stride=1, padding=1, residual=True),
			Conv2d(32, 32, kernel_size=3, stride=1, padding=1, residual=True),

			Conv2d(32, 64, kernel_size=3, stride=(3, 1), padding=1),
			Conv2d(64, 64, kernel_size=3, stride=1, padding=1, residual=True),
			Conv2d(64, 64, kernel_size=3, stride=1, padding=1, residual=True),

			Conv2d(64, 128, kernel_size=3, stride=3, padding=1),
			Conv2d(128, 128, kernel_size=3, stride=1, padding=1, residual=True),
			Conv2d(128, 128, kernel_size=3, stride=1, padding=1, residual=True),

			Conv2d(128, 256, kernel_size=3, stride=(3, 2), padding=1),
			Conv2d(256, 256, kernel_size=3, stride=1, padding=1, residual=True),

			Conv2d(256, 512, kernel_size=3, stride=1, padding=0),
			Conv2d(512, 512, kernel_size=1, stride=1, padding=0)
		)

	def forward(self, audioSequences):
		# audioSequences :: (B, 1, 80, 16) or (B, T, 1, 80, 16)
		input_dim_size = len(audioSequences.shape)
		B = audioSequences.shape[0]
		audioSequences = audioSequences.view(B, self.dIn, 80, 16)

		audioEmbedding = self.model(audioSequences)
		return audioEmbedding