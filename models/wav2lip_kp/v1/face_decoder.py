import torch as tr
import torch.nn as nn
from ...base_models.conv import Conv2d, Conv2dTranspose

class FaceDecoder(nn.Module):
	def __init__(self):
		super(FaceDecoder, self).__init__()

		self.model = nn.ModuleList([
			nn.Sequential(
				Conv2d(512, 512, kernel_size=1, stride=1, padding=0)
			),
			nn.Sequential(
				Conv2dTranspose(1024, 512, kernel_size=3, stride=1, padding=0),
				Conv2d(512, 512, kernel_size=3, stride=1, padding=1, residual=True)
			), # 3, 3
			nn.Sequential(
				Conv2dTranspose(1024, 512, kernel_size=3, stride=2, padding=1, output_padding=1),
				Conv2d(512, 512, kernel_size=3, stride=1, padding=1, residual=True),
				Conv2d(512, 512, kernel_size=3, stride=1, padding=1, residual=True)
			), # 6, 6
			nn.Sequential(
				Conv2dTranspose(768, 384, kernel_size=3, stride=2, padding=1, output_padding=1),
				Conv2d(384, 384, kernel_size=3, stride=1, padding=1, residual=True),
				Conv2d(384, 384, kernel_size=3, stride=1, padding=1, residual=True)
			), # 12, 12
			nn.Sequential(
				Conv2dTranspose(512, 256, kernel_size=3, stride=2, padding=1, output_padding=1),
				Conv2d(256, 256, kernel_size=3, stride=1, padding=1, residual=True),
				Conv2d(256, 256, kernel_size=3, stride=1, padding=1, residual=True)
			), # 24, 24
			nn.Sequential(
				Conv2dTranspose(320, 128, kernel_size=3, stride=2, padding=1, output_padding=1),
				Conv2d(128, 128, kernel_size=3, stride=1, padding=1, residual=True),
				Conv2d(128, 128, kernel_size=3, stride=1, padding=1, residual=True)
			), # 48, 48
			nn.Sequential(
				Conv2dTranspose(160, 64, kernel_size=3, stride=2, padding=1, output_padding=1),
				Conv2d(64, 64, kernel_size=3, stride=1, padding=1, residual=True),
				Conv2d(64, 64, kernel_size=3, stride=1, padding=1, residual=True)
			) # 96, 96
			])

		self.output_block = nn.Sequential(
			Conv2d(80, 32, kernel_size=3, stride=1, padding=1),
			nn.Conv2d(32, 3, kernel_size=1, stride=1, padding=0),
		)

	def forward(self, faceEmbeddingList, audioEmbedding):
		assert len(audioEmbedding.shape) == 4

		x = audioEmbedding
		for i, f in enumerate(self.model):
			x = f(x)
			# -1, -2, .. 0
			x = tr.cat((x, faceEmbeddingList[-i - 1]), dim=1)

		outputs = self.output_block(x)
		outputs = tr.sigmoid(outputs)
		return outputs
