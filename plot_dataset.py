import matplotlib.pyplot as plt
import numpy as np
from neural_wrappers.utilities import npGetInfo, minMax
from plot_utils import Viridis, kpsToImage
from media_processing_lib.image import toImage, tryWriteImage, imgResize
from tqdm import trange

def plot_dataset(generator):
	cnt = 0
	for i in trange(len(generator)):
		(data, labels), _ = next(generator)
		MB, D, H, W = data["targetFrame"].shape
		print("Batch size: %d" % MB)

		mask = np.ones((H, W))
		mask[H // 2:] = 0
		data["targetGenerated"] = data["targetFrame"] * 0
		data["targetMasked"] = data["targetFrame"] * mask

		if "targetKeypoints" in data:
			data["sourceKeypoints"] = data["sourceKeypoints"] * (H, W)
			data["targetKeypoints"] = data["targetKeypoints"] * (H, W)

		if not data["targetContext"] is None:
			data["targetFrame"] = np.concatenate([data["targetFrame"][:, None], data["targetContext"]], axis=1)
			data["targetFrame"] = np.concatenate([data["targetFrame"][:, i] for i in range(data["targetFrame"].shape[1])], axis=3)
			data["sourceFrame"] = np.concatenate([data["sourceFrame"][:, None], data["sourceContext"]], axis=1)
			data["sourceFrame"] = np.concatenate([data["sourceFrame"][:, i] for i in range(data["sourceFrame"].shape[1])], axis=3)
			data["targetMasked"] = np.concatenate([data["targetMasked"][:, None], data["targetContext"] * mask], axis=1)
			data["targetMasked"] = np.concatenate([data["targetMasked"][:, i] for i in range(data["targetMasked"].shape[1])], axis=3)
			data["targetMel"] = np.concatenate([data["targetMel"][:, None], data["targetMelContext"]], axis=1)
			data["targetMel"] = np.concatenate([data["targetMel"][:, i] for i in range(data["targetMel"].shape[1])], axis=2)

			if "targetKeypoints" in data:
				data["sourceContextKeypoints"] = data["sourceContextKeypoints"] * (H, W)
				data["targetContextKeypoints"] = data["targetContextKeypoints"] * (H, W)
				data["sourceKeypoints"] = np.concatenate([data["sourceKeypoints"][:, None], data["sourceContextKeypoints"]], axis=1)
				data["targetKeypoints"] = np.concatenate([data["targetKeypoints"][:, None], data["targetContextKeypoints"]], axis=1)

		for k in data:
			if data[k] is None:
				print(k, "None")
			else:
				print(k, npGetInfo(data[k]))

		for j in range(MB):
			thisData = {k : (data[k][j] if not data[k] is None else None) for k in data}
			source = toImage(thisData["sourceFrame"])
			target = toImage(thisData["targetFrame"])
			targetMasked = toImage(thisData["targetMasked"])
			targetMel = imgResize(toImage(Viridis(thisData["targetMel"])), height=H, width=targetMasked.shape[1], \
				interpolation="bilinear")
			items = [source, target, targetMasked, targetMel]
			if "targetKeypoints" in data:
				sourceKeypoints = np.concatenate(kpsToImage(thisData["sourceKeypoints"], (H, W)), axis=1)
				targetKeypoints = np.concatenate(kpsToImage(thisData["targetKeypoints"], (H, W)), axis=1)
				items.extend([sourceKeypoints, targetKeypoints])

			stack = np.concatenate(items, axis=0)
			cnt += 1
			tryWriteImage(stack, "%d.png"  % cnt)

			if cnt == 100:
				exit()