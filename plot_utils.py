import numpy as np
import cv2
from pathlib import Path
from neural_wrappers.callbacks import Callback
from neural_wrappers.utilities import minMax
from media_processing_lib.image import tryWriteImage, toImage, imgResize
from matplotlib.cm import hot, viridis

def Hot(x):
	x = hot(minMax(x))[..., 0 : 3]
	return x

def Viridis(x):
	return viridis(minMax(x))[..., 0 : 3]

def kpsToImage(x, resolution):
	H, W = resolution
	radius = max(2, min(H, W) // 200)
	thickness = -1
	res = []
	for i in range(len(x)):
		imgKeypoints = np.int32(x[i])
		frame = np.ones((H, W, 3), dtype=np.uint8) * 255
		for kp_i, kp_j in zip(imgKeypoints[:, 0], imgKeypoints[:, 1]):
			frame = cv2.circle(frame, (kp_i, kp_j), radius, (0, 0, 255), thickness)
		res.append(frame)
	res = np.array(res)
	return res

def doOnePlot(results, labels):
	H, W = labels["targetFrame"].shape[1 : 3]
	diff = np.abs(labels["targetFrame"] - results["targetGenerated"]).sum(axis=0)
	mel = Viridis(labels["targetMel"])
	sourceFrame = toImage(labels["sourceFrame"])
	targetFrame = toImage(labels["targetFrame"])
	targetMasked = toImage(results["targetMasked"])
	targetGenerated = toImage(results["targetGenerated"])
	diff = toImage(Hot(diff))
	targetMel = imgResize(toImage(mel), height=H, width=W, interpolation="bilinear")
	items = [sourceFrame, targetFrame, targetMasked, targetGenerated, diff, targetMel]
	if "sourceKeypoints" in labels:
		kp = kpsToImage(labels["sourceKeypoints"][None] * (H, W), (H, W))[0]
		items.insert(1, toImage(kp))

	items = np.concatenate(items, axis=1)
	return items

class RandomPlotEpoch(Callback):
	def __init__(self, iterationsPerEpoch):
		super().__init__()
		self.iterationsPerEpoch = iterationsPerEpoch

	def onEpochStart(self, **kwargs):
		Path("samples/%d" % kwargs["epoch"]).mkdir(exist_ok=True, parents=True)
		self.currentEpoch = kwargs["epoch"]

	def plotFn(self, results, labels):
		MB = len(results["targetGenerated"])
		for j in range(MB):
			thisResults = {k : results[k][j] for k in ["targetMasked", "targetGenerated"]}
			thisLabels = {k : (labels[k][j] if not labels[k] is None else None) for k in labels}
			stack = doOnePlot(thisResults, thisLabels)
			tryWriteImage(stack, "samples/%d/%d.png" % (self.currentEpoch, j))

	def onIterationEnd(self, results, labels, **kwargs):
		if kwargs["iteration"] == 0:
			self.plotFn(results, kwargs["data"])
