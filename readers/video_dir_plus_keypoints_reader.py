import os
import numpy as np
from pathlib import Path
from functools import partial

from neural_wrappers.readers import DatasetReader
from vke.face import imgGetFaceKeypoints
from media_processing_lib.video import tryReadVideo

from .utils import preprocessVideo, normalizeVideo
from .video_dir_reader import fGetter as baseGetter

def mergeKpFn(items):
	n = len(items)
	Keys = ["sourceFrame", "targetFrame", "sourceContext", "targetContext", \
		"targetMel", "targetMelContext", "sourceFrameIx", "targetFrameIx", \
		"sourceKeypoints", "sourceContextKeypoints", "targetKeypoints", "targetContextKeypoints"]
	catItems = {k : [] for k in Keys}
	for i in range(n):
		item = items[i]
		if item is None:
			continue
		for k in Keys:
			if k in item.keys() and not item[k] is None:
				catItems[k].append(item[k])

	resItems = {}
	for k in catItems:
		# None troubles
		if len(catItems[k]) == 0:
			resItems[k] = None
			continue
		# BatchNorm troubles
		if len(catItems[k]) == 1:
			catItems[k].append(catItems[k][0])
		resItems[k] = np.stack(catItems[k], axis=0)
	return resItems, resItems

def tryImgGetFaceKeypoints(image):
	item = imgGetFaceKeypoints(image, faceLib="face_alignment")
	if item is None:
		item = np.zeros((68, 2), dtype=np.uint32)
	return item

def fGetter(dataset, index, obj):
	item = baseGetter(dataset, index, obj)
	if item is None:
		return None
	video = tryReadVideo(dataset[index], vidLib="pims")

	targetFrameIx, sourceFrameIx = item["indexes"]["targetFrameIx"], item["indexes"]["sourceFrameIx"]
	context = obj.context

	sourceKeypoints = tryImgGetFaceKeypoints(item["frames"]["sourceFrame"])
	targetKeypoints = tryImgGetFaceKeypoints(item["frames"]["targetFrame"])
	item["keypoints"] = {"sourceKeypoints" : sourceKeypoints, "targetKeypoints" : targetKeypoints}
	if context > 0:
		sourceContextKeypoints = np.stack([tryImgGetFaceKeypoints(f) for f in item["frames"]["sourceContext"]])
		targetContextKeypoints = np.stack([tryImgGetFaceKeypoints(f) for f in item["frames"]["targetContext"]])
		item["keypoints"]["sourceContextKeypoints"] = sourceContextKeypoints
		item["keypoints"]["targetContextKeypoints"] = targetContextKeypoints
	return item

def coord_to_mask(coords, H, W):
	assert len(coords.shape) == 3 and coords.shape[-1] == 2, "Found: %s" % str(coords.shape)
	B = coords.shape[0]
	res = np.zeros((B, H, W), dtype=np.float32)
	coords = coords.astype(np.int32)
	Y, X = coords[..., 0], coords[..., 1]
	for i in range(B):
		res[i, Y[i], X[i]] = 1
	return res

def normalizeKPVideo(item):
	normedItem = normalizeVideo(item)
	if normedItem is None:
		return None

	H, W = item["frames"]["sourceFrame"].shape[0 : 2]
	# Convert from coordinate space to [0 : 1] so we can produce resolution-agnostic optical flows for kps.
	for key in item["keypoints"]:
		normedItem[key] = item["keypoints"][key] / (H, W)
	return normedItem

class VideoDirPlusKeypointsReader(DatasetReader):
	def __init__(self, baseReader):
		self.baseReader = baseReader

		super().__init__(
			dataBuckets = {"data" : ["video"]},
			dimGetter = {"video" : partial(fGetter, obj=self)},
			dimTransform = {"data" : {"video" : normalizeKPVideo}})
		self.datasetFormat.isCacheable = True

	def getDataset(self):
		return self.videoList

	def __len__(self):
		return len(self.videoList)

	def __getitem__(self, index):
		item = super().__getitem__(index)
		item = item["data"]["video"]
		return item

	def __getattr__(self, key):
		return getattr(self.baseReader, key)

	def __str__(self):
		Str = "[Wav2Lip::VideoDirPlusKeypointsReader]"
		Str += "\n - Dataset path: %s" % self.videoDirPath
		Str += "\n - Num videos: %d. Resolution: %s" % (len(self.videoList), str(self.resolution))
		Str += "\n - Audio sample rate: %d. Audio window size: %ds" % (self.sampleRate, self.audioWindowSize)
		Str += "\n - Context: %d frames." % self.context
		return Str

	def __cache__(self):
		Str = "lipgen_kp/res-%dx%d/sr-%d/ctx-%d/audio-%ds" % (self.resolution[0], self.resolution[1], \
			self.sampleRate, self.context, self.audioWindowSize)
		return Str
