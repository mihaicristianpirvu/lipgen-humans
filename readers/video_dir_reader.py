import os
import numpy as np
from pathlib import Path
from functools import partial

from neural_wrappers.readers import DatasetReader
from media_processing_lib.video import tryReadVideo
from media_processing_lib.audio import tryReadAudio
from vke.face import vidGetFaceBbox

from .utils import preprocessVideo, normalizeVideo

def fGetter(dataset, index, obj):
	assert isinstance(index, (int, np.int32, np.int64, np.int16))
	path = dataset[index]
	video = tryReadVideo(path, vidLib="pims")
	bbox = vidGetFaceBbox(video, faceLib="face_alignment")
	wav = tryReadAudio(path, sampleRate=obj.sampleRate)

	assert len(video) == len(bbox)
	if len(video) / video.fps < obj.minVidLength:
		return None

	item = preprocessVideo(video, wav, bbox, obj.resolution, obj.audioWindowSize, obj.context)
	return item

class VideoDirReader(DatasetReader):
	def __init__(self, videoDirPath:str, context:int):
		self.videoDirPath = os.path.abspath(os.path.realpath(videoDirPath))
		self.videoList = [str(x) for x in Path(self.videoDirPath).glob("*.mp4")]
		assert len(self.videoList) > 0

		self.resolution = 96, 96 # height, width
		self.context = context # +/- frames left/right
		self.audioWindowSize = 1 # seconds
		self.minVidLength = 2 # seconds
		self.sampleRate = 16000 # hz

		super().__init__(
			dataBuckets = {"data" : ["video"]},
			dimGetter = {"video" : partial(fGetter, obj=self)},
			dimTransform = {"data" : {"video" : normalizeVideo}})
		self.datasetFormat.isCacheable = True

	def getDataset(self):
		return self.videoList

	def __len__(self):
		return len(self.videoList)

	def __getitem__(self, index):
		item = super().__getitem__(index)
		item = item["data"]["video"]
		return item

	def __str__(self):
		Str = "[Wav2Lip::VideoDirReader]"
		Str += "\n - Dataset path: %s" % self.videoDirPath
		Str += "\n - Num videos: %d. Resolution: %s" % (len(self.videoList), str(self.resolution))
		Str += "\n - Audio sample rate: %d. Audio window size: %ds" % (self.sampleRate, self.audioWindowSize)
		Str += "\n - Context: %d frames." % self.context
		return Str

	def __cache__(self):
		Str = "lipgen/res-%dx%d/sr-%d/ctx-%d/audio-%ds" % (self.resolution[0], self.resolution[1], self.sampleRate, \
			self.context, self.audioWindowSize)
		return Str
