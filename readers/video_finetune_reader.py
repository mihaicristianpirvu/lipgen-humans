from neural_wrappers.readers import DatasetReader
from media_processing_lib.video import tryReadVideo
from media_processing_lib.audio import tryReadAudio
from vke.face import vidGetFaceBbox
from .utils import preprocessVideo, normalizeVideo

def fGetter(dataset, index):
	item = preprocessVideo(dataset.video, dataset.wav, dataset.bbox, dataset.resolution, \
		dataset.audioWindowSize, dataset.context, seed=index)
	return item

class VideoFineTuneReader(DatasetReader):
	# @param[in] videoPath Path to the desired video to fine tune on
	# @param[in] context The number of left/right frames for each frame
	# @param[in] N The number of frames used for fine tuning
	def __init__(self, videoPath:str, context:int, N:int):
		self.N = N
		self.resolution = 96, 96 # height, width
		self.context = context # +/- frames left/right
		self.audioWindowSize = 1 # seconds
		self.minVidLength = 2 # seconds
		self.sampleRate = 16000 # hz

		self.video = tryReadVideo(videoPath, vidLib="pims")
		self.bbox = vidGetFaceBbox(self.video, faceLib="face_alignment")
		self.wav = tryReadAudio(videoPath, sampleRate=self.sampleRate)
		assert len(self.video) == len(self.bbox)
		assert N <= len(self.video), "Use at most the number of frames (N=%d)" % len(self.video)
		assert len(self.video) / self.video.fps > self.minVidLength

		super().__init__(
			dataBuckets = {"data" : ["video"]},
			dimGetter = {"video" : fGetter},
			dimTransform = {"data" : {"video" : normalizeVideo}}
		)
		self.datasetFormat.isCacheable = True

	def getDataset(self):
		return self

	def __len__(self):
		return self.N

	def __getitem__(self, index):
		item = super().__getitem__(index)
		item = item["data"]["video"]
		return item